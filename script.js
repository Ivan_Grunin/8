/* Задание на урок:

1) Автоматизировать вопросы пользователю про фильмы при помощи цикла

2) Сделать так, чтобы пользователь не мог оставить ответ в виде пустой строки,
отменить ответ или ввести название фильма длинее, чем 50 символов. Если это происходит - 
возвращаем пользователя к вопросам опять

3) При помощи условий проверить  personalMovieDB.count, и если он меньше 10 - вывести сообщение
"Просмотрено довольно мало фильмов", если от 10 до 30 - "Вы классический зритель", а если больше - 
"Вы киноман". А если не подошло ни к одному варианту - "Произошла ошибка"

4) Потренироваться и переписать цикл еще двумя способами*/

'use strict';

// Код возьмите из предыдущего домашнего задания

const numberOfFilms = +prompt("Сколько фильмов вы уже посмотрели?", "");

const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
}

/*-----1 for----*/

for (let i = 0; i < 2; i++) {
    const nameOfFilm = prompt("Один из последних просмотренных фильмов?", ""),
          rateFilm = prompt("На сколько оцените его?", "");

    if (nameOfFilm != null && rateFilm != null && nameOfFilm != '' && rateFilm != '' && nameOfFilm.length <= 50) {
        personalMovieDB.movies[nameOfFilm] = rateFilm;
        console.log('correct');
    } else {
        console.log('not correct');
        i--;
    }
}


/*-----2 while----*/
/*
let i = 0;
while (i < 2) {
    const nameOfFilm = prompt("Один из последних просмотренных фильмов?", ""),
          rateFilm = prompt("На сколько оцените его?", "");

    if (nameOfFilm != null && rateFilm != null && nameOfFilm != '' && rateFilm != '' && nameOfFilm.length <= 50) {
        personalMovieDB.movies[nameOfFilm] = rateFilm;
        console.log('correct');
        i++;
    } else {
        console.log('not correct');
    }
 }
*/

/*-----3 do while----*/
/*
let i = 0;
do {
    const nameOfFilm = prompt("Один из последних просмотренных фильмов?", ""),
          rateFilm = prompt("На сколько оцените его?", "");
          if (nameOfFilm != null && rateFilm != null && nameOfFilm != '' && rateFilm != '' && nameOfFilm.length <= 50) {
            personalMovieDB.movies[nameOfFilm] = rateFilm;
            console.log('correct');
            i++;
        } else {
            console.log('not correct');
        }
} while (i < 2);
*/
if (personalMovieDB.count < 10) {
    alert("Просмотрено довольно мало фильмов");
} else if (personalMovieDB.count >= 10 && personalMovieDB.count < 30) {
    alert("Вы классический зритель");
} else if (personalMovieDB.count >= 30) {
    alert("Вы киноман");
} else {
    alert("Произошла ошибка");
}

console.log(personalMovieDB); 